<!DOCTYPE html>
<html>
 <head>
	<link rel="stylesheet" href="test.css">
	<meta charset="utf-8">
	<title> Inscription</title>
 </head>

 
 <body>
  <header><img src="kaggle_logo.png" alt="Kaggle"> </span>
</header>

<h1>INSCRIVEZ-VOUS POUR NOUS REJOINDRE !!!</h1>
<br /><br /><br />
<?phpsession_start();?>
<?php
if (!empty($_POST)){
	
$identifiant=$_POST['identifiant'];
$motdepasse=$_POST['motdepasse'];
$nom=$_POST['nom'];
$prenom=$_POST['prenom'];
$email=$_POST['email'];

	if (!preg_match('/^[a-zA-Z0-9]+$/',$_POST['nom'])){
echo "<h3 class=error>Votre nom n'est pas valide</h3>";}
else{
		if (!preg_match('/^[a-zA-Z0-9]+$/',$_POST['prenom'])){
echo "<h3 class=error>Votre prenom n'est pas valide</h3>";}
else{
			if (!preg_match('/^[a-zA-Z0-9_]+$/',$_POST['identifiant'])){
echo "<h3 class=error>Votre identifiant n'est pas valide</h3>";}
else{
if ($_POST['motdepasse']!=$_POST['motdepasseConfirm']){
	echo "<h3 class=error>Vous devez entrer des mots de passes valides et identiques</h3>";

	}
else
{
	require_once('param.inc.php');
 $mysqli = new mysqli( $host, $login,$password,$dbname);
 if ($mysqli->connect_errno) {
 echo "Echec lors de la connexion à MySQL : (" . $mysqli->
connect_errno . ") " . $mysqli->connect_error;
 }else{
	 

//---------------------------------on filtre les données saisies par l'utilisateur
$identifiantf = filter_var($identifiant, FILTER_SANITIZE_STRING);
$nom = filter_var($nom, FILTER_SANITIZE_STRING);
$prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);

//---------------------------------Si l'identifiant filtré est différent de celui saisi par l'utilisateur, on affiche un message d'erreur 
if($identifiantf!=$identifiant){
	echo "<h3 class=error>Votre identifiant n'est pas valide</h3>";}

else{
	//---------on vérifie si l'identifiant est déja utilisé
		 $query= mysqli_query($mysqli,"SELECT * FROM comptes WHERE identifiant='$identifiantf'");
	$numrows = mysqli_num_rows($query);
	if($numrows!=0){
	echo "<h3 class=error>Votre identifiant existe déjà</h3>";}
	//---------si l'identifiant n'est pas déja utilisé, on insère les données entrées par l'utilisateur dans la base de données
		else{$mysqli->query("INSERT INTO comptes(nom,prenom,email,identifiant,mdp) VALUES ('$nom','$prenom','$email','$identifiantf','$motdepasse') ");
 echo ' <h3 class=nerror>Bonjour '. htmlentities($_POST['identifiant']) .'. Vous etes bien inscrit</h3>';
 }
	
}

	}
 

 }
	
}
}

}}
?>
<form method="post" >
 
   <fieldset>
       <legend>Vos informations</legend> <!-- Titre du fieldset --> 

	   <!-- label et zone de texte pour nom --> 
       <label for="nom">Nom : </label><br />
       <input type="text" name="nom" id="nom" required /><br /><br />

	   <!-- label et zone de texte pour prenom --> 
       <label for="prenom">Prénom: </label><br />
       <input type="text" name="prenom" id="prenom" required/><br /><br />
	   
	   <!-- label et choix de valeur pour sexe --> 
	   <label for="sexe">Sexe:  </label><span />
       <input type="radio" name="sexe" value="masculin" id="masculin" /> <label for="masculin" required/>M   </label>
       <input type="radio" name="sexe" value="feminin" id="feminin" /> <label for="feminin" required/>F</label><br /><br />
	   
	   <!-- label et zone de texte pour email --> 
	    <label for="email">E-mail: </label><br />
       <input type="email" name="email" id="email" placeholder="toto@xxx.xx" required/><br /><br />

	   <!-- label et zone de texte pour identifiant --> 
	   <label for="identifiant">Identifiant</label><br />
       <input type="text" name="identifiant" id="identifiant" required/><br /><br />
	   
	   <!-- label et zone de texte pour mot de passe --> 
	     <label for="motdepasse">Mot de passe</label><br />
       <input type="password" name="motdepasse" id="motdepasse" required/><br /><br />
	   
	   <!-- label et zone de texte pour confirmation mot de passe --> 
	      <label for="motdepasseConfirm">Confirmer mot de passe</label><br />
       <input type="password" name="motdepasseConfirm" id="motdepasseConfirm" required/><br /><br />
	  

   <!-- bouton envoyer --> 
	   <input type="submit" value="Envoyer" />
   </fieldset>

</form>
</body>

</html> 
<?php
echo "<h2><a href='index1.php'> Revenir a la page d'accueil </a></h2>"	;
?>