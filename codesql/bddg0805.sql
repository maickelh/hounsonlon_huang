-- phpMyAdmin SQL Dump
-- version 4.2.8.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 16 Décembre 2015 à 13:25
-- Version du serveur :  10.0.13-MariaDB
-- Version de PHP :  5.5.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bddg0805`
--

-- --------------------------------------------------------

--
-- Structure de la table `candidature`
--

CREATE TABLE IF NOT EXISTS `candidature` (
`id` int(11) NOT NULL,
  `candidat` varchar(25) NOT NULL COMMENT 'nom de cadidature pour postuler',
  `chef_equipe` varchar(25) NOT NULL COMMENT 'le nom de chef d''equipe '
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='pour postuler candidature envoie son nom a chef equipe';

-- --------------------------------------------------------

--
-- Structure de la table `comptes`
--

CREATE TABLE IF NOT EXISTS `comptes` (
`id` int(11) NOT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL COMMENT 'en forme xxx@xxx.xxx',
  `identifiant` varchar(25) DEFAULT NULL,
  `mdp` varchar(25) DEFAULT NULL COMMENT 'mot de passe',
  `session` int(11) DEFAULT '0' COMMENT '0==visiteur 1==epquipier 2==chef d''equipe'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='les comptes de membre';

--
-- Contenu de la table `comptes`
--

INSERT INTO `comptes` (`id`, `nom`, `prenom`, `email`, `identifiant`, `mdp`, `session`) VALUES
(1, 'ah', 'ah', 'ah@ha.fr', 'ah', '123456', 2),
(2, 'mh', 'mh', 'mh@mh.com', 'mh', '123456', 2),
(3, 'test1', 'test1', 'test1@123.com', 'test1', '1', 1),
(4, 'test2', 'test2', 'test1@123.com', 'test2', '1', 1),
(5, 'test3', 'test3', 'test1@123.com', 'test3', '1', 1),
(6, 'test4', 'test4', 'test1@123.com', 'test4', '1', 1),
(7, 'test5', 'test5', 'test1@123.com', 'test5', '1', 0),
(8, 'test6', 'test6', 'text1@123.com', 'text6', '1', 0);

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
`id` int(11) NOT NULL,
  `nom_equipe` varchar(25) NOT NULL,
  `chef_equipe` varchar(25) NOT NULL COMMENT 'identifiant de chef',
  `membre_1` varchar(25) DEFAULT 'vide' COMMENT 'identifiant de membre',
  `membre_2` varchar(25) DEFAULT 'vide' COMMENT 'identifiant de membre',
  `membre_3` varchar(25) NOT NULL DEFAULT 'vide' COMMENT 'identifiant de membre',
  `membre_4` varchar(25) NOT NULL DEFAULT 'vide' COMMENT 'identifiant de membre'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `equipe`
--

INSERT INTO `equipe` (`id`, `nom_equipe`, `chef_equipe`, `membre_1`, `membre_2`, `membre_3`, `membre_4`) VALUES
(1, 'groupetext1', 'ah', 'text1', 'text2', 'text3', 'text4'),
(2, 'groupetext2', 'mh', 'vide', 'vide', 'vide', 'vide');

-- --------------------------------------------------------

--
-- Structure de la table `fichiers`
--

CREATE TABLE IF NOT EXISTS `fichiers` (
`id` int(11) NOT NULL,
  `nom_fichier` varchar(100) DEFAULT NULL,
  `chef_equipe` varchar(25) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `fichiers`
--

INSERT INTO `fichiers` (`id`, `nom_fichier`, `chef_equipe`) VALUES
(1, 'compte rendu text2.txt', 'ah'),
(2, 'deploiement.pdf', 'ah');

-- --------------------------------------------------------

--
-- Structure de la table `reunion`
--

CREATE TABLE IF NOT EXISTS `reunion` (
`id` int(11) NOT NULL,
  `nom_equipe` varchar(25) NOT NULL DEFAULT 'error',
  `Jour` date DEFAULT NULL COMMENT 'date',
  `heure` time(6) DEFAULT NULL COMMENT 'heure'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reunion`
--

INSERT INTO `reunion` (`id`, `nom_equipe`, `Jour`, `heure`) VALUES
(2, 'groupetext1', '2015-12-19', '05:00:00.000000');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `candidature`
--
ALTER TABLE `candidature`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comptes`
--
ALTER TABLE `comptes`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `equipe`
--
ALTER TABLE `equipe`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fichiers`
--
ALTER TABLE `fichiers`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reunion`
--
ALTER TABLE `reunion`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `candidature`
--
ALTER TABLE `candidature`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `comptes`
--
ALTER TABLE `comptes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `equipe`
--
ALTER TABLE `equipe`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `fichiers`
--
ALTER TABLE `fichiers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `reunion`
--
ALTER TABLE `reunion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
