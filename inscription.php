
<h1>INSCRIVEZ-VOUS POUR NOUS REJOINDRE !!!</h1>
<br /><br /><br />
<form method="post" action="inscrits.php">
 
   <fieldset>
       <legend>Vos informations</legend> <!-- Titre du fieldset --> 

       <label for="nom">Nom</label><br />
       <input type="text" name="nom" id="nom" required /><br /><br />

       <label for="prenom">Prénom</label><br />
       <input type="text" name="prenom" id="prenom" required/><br /><br />
	   
	   <label for="sexe">Sexe:  </label><span />
       <input type="radio" name="sexe" value="masculin" id="masculin" /> <label for="masculin">M   </label>
       <input type="radio" name="sexe" value="feminin" id="feminin" /> <label for="feminin">F</label><br /><br />
 

       <label for="pays">Nationalité</label><br />
       <select name="pays" id="pays">
	   <option value="algerie">Algérie</option>
           <option value="benin" selected>Bénin</option>
		 
           <option value="cameroun">Cameroun</option>
		   <option value="chine">Chine</option>
           <option value="france">France</option>
		   <option value="maroc">Maroc</option>
           <option value="senegal">Sénégal</option>
		<option value="japon">Japon</option>
       </select>
<br /><br />

  <label for="pays">Niveau de scolarité</label><br />
       <select name="niveau" id="niveau">
	   <option value="bac+1">Bac+1</option>
           <option value="bac+2" selected>Bac+2</option>
		 
           <option value="bac+3">Bac+3</option>
		   <option value="bac+4">Bac+4</option>
           <option value="bac+5">Bac+5</option>
		   <option value="fontanet">Fontanet</option>
          
       </select>
<br /><br />

       <label for="email">E-mail</label><br />
       <input type="email" name="email" id="email" placeholder="toto@xxx.xx" required/><br /><br />
	   
	   <label for="email">Confirmer votre e-mail</label><br />
       <input type="email" name="email" id="email" placeholder="toto@xxx.xx" required/><br /><br />
	   
	   <label for="num">Numéro de téléphone</label><br />
       <input type="tel" name="num" id="num" required/><br /><br />
	   
	   <label for="identifiant">Identifiant</label><br />
       <input type="text" name="identifiant" id="identifiant" required/><br /><br />
	   
	     <label for="motdepasse">Mot de passe</label><br />
       <input type="password" name="motdepasse" id="motdepasse" required/><br /><br />
	   
	      <label for="motdepasseConfirm">Confirmer mot de passe</label><br />
       <input type="password" name="motdepasseConfirm" id="motdepasseConfirm" required/><br /><br />
	  
	   <br /><br />

	   <input type="submit" value="Valider" />
   </fieldset>

</form>

